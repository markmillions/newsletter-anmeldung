# Newsletter Anmeldung

**Das Double-Opt-In Verfahren:**

Beim **Double Opt-In**-Verfahren (doppelter Beitritt) erteilt der Empfänger dem Absender die Erlaubnis ihm Werbe-E-Mails zuzusenden. Der Empfänger erhält eine E-Mail mit einem Double-Opt-In-Link, den er als Bestätigung klicken muss, um für den Newsletter angemeldet zu werden.

In der schematischen Darstellung wird der genaue Ablauf des Verfahrens beschrieben und benötigte Formulare als Wireframes dargestellt.


![Double Opt In](Double Opt In.png)


# Newsletter Abmeldung

**Das Single-Opt-Out Verfahren:**

Das **Single-opt-Out** ist ein Abmeldeverfahren. Der Empfänger trägt sich aus einem Verteiler aus. Nach der Abmeldung löscht der Anbieter die Adresse des Abmeldenden aus seinem Verteiler und informiert diesen über die erfolgreiche Abmeldung.

In der schematischen Darstellung wird der genaue Ablauf des Verfahrens beschrieben und benötigte Formulare als Wireframes dargestellt.


![Single Opt Out](Single Opt Out.png)